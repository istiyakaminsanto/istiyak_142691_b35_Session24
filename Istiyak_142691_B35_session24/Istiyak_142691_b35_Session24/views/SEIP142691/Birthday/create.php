<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <link href="https://fonts.googleApIs.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../../resource/assets/js/bootstrap.min.js">
    <link rel="stylesheet" href="../../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../../resource/assets/css/style.css">

</head>
<body>
<div class="container">
    <div class="container-fluid">

        <header>
            <div class="logo_area">
                <p class="logo">Atomic project</p>
                <h3 class="subLogo">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li><a href="../BookTitle/create.php">BOOK TITLE</a></li>
                    <li class="active"><a href="../Birthday/create.php">BIRTHDAY</a></li>
                    <li><a href="#">GENDER</a></li>
                    <li><a href="#">EMAIL</a></li>
                    <li><a href="#">HOBBIES</a></li>
                    <li><a href="#">CITY</a></li>
                    <li><a href="#">PROFILE PICTURE</a></li>
                    <li><a href="#">SUMMARY OF ORGNAIZATION</a></li>
                </ul>

            </nav>
        </header>
        <div><img src="../../../../resource/img/booktitle.jpg" alt=""></div>
        <p class="title">Birthday</p>

        <fieldset class="control-group ">
            <legend>Add your Date of Birth</legend>
            <form action="store.php" method="post" class="form-inline">
                <div class="input_form">
                    <label for="Date">Date of Birth&nbsp;</label>
                    <input type="datetime" id="Date" class="input-xxlarge" name="birthday">
                </div>
                <div class="input_form">
                    <label for="Author">Author Name</label>
                    <input type="datetime" id="Date" class="input-xxlarge" name="author_name">
                </div>
                <div>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    <input type="submit" id="Date" class="btn btn-success" value="submit">
                </div>
            </form>
        </fieldset>

    </div>
    <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
</div>
</body>
</html>