<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class BookTitle extends DB{
    public $id="";
    public $book_title="";
    public $author_name="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('book_title',$postVariableData)){
            $this->book_title = $postVariableData['book_title'];
        }

        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
    }



    public function store(){

    $arrData = array( $this->book_title, $this->author_name);

    $sql = "Insert INTO book_title(book_title, author_name) VALUES (?,?)";
    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

     if($result)
            Message::message("<h3>Success! Data Has Been Inserted Successfully :)</h3>");
     else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method







    public function index(){
        echo "I am inside index method of <b>BookTitle</b> Class";
    }




}

?>

