<?php
namespace App\Birthday;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Birthday extends DB{
    public $id="";
    public $birthday="";
    public $author_name="";

    public function __construct(){
        parent:: __construct();
        if(!isset($_SESSION)) session_start();
    }

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('birthday',$postVariableData)){
            $this->birthday = $postVariableData['birthday'];
        }

        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
    }



    public function store(){

        $arrData = array( $this->birthday, $this->author_name);

        $sql = "Insert INTO birthday(birthday, author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method







    public function index(){
        
    }




}

?>

